# front
Project name : Bilbao-Azoka

Nombre y apedido : AJAMI MOHAMED YASSINE 
Mail : ajami.my@gmail.com
Móvil : 631 12 55 

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
