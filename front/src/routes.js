import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    routes: [
        {
            path: '/Bilbao-District',
            component: function (resolve) {
                require(['@/components/BilbaoDistrict/BilbaoDistrictPage.vue'], resolve)
            },
        },
        {
            path: '/Product-List',
            component: function (resolve) {
                require(['@/components/ProductList/ProductListPage.vue'], resolve)
            },
        },

        {
            path: '/product-List/searchedProductsByName/:searchedProductName',
            component: function (resolve) {
                require(['@/components/ProductList/ProductListPage.vue'], resolve)
            },
        },

        {
            path: '/Product-List/Principal-Categories/:searchedSubCategory',
            component: function (resolve) {
                require(['@/components/ProductList/ProductListPage.vue'], resolve)
            },
        },
        {
            path: '/Product-List/filtred-by-Principal-Categories/:filtredPrincipalCategory',
            component: function (resolve) {
                require(['@/components/ProductList/ProductListPage.vue'], resolve)
            },
        },


        {
            path: '/Product-Details/:searchedProduct',
            component: function (resolve) {
                require(['@/components/ProductDetails/ProductDetailsPage.vue'], resolve)
            },
        },
        {
            path: '/Shop-Details/:searchedShop',
            component: function (resolve) {
                require(['@/components/ShopDetails/ShopDetailsPage.vue'], resolve)
            },
        },


        {
            path: '/Shop-List',
            component: function (resolve) {
                require(['@/components/ShopList/ShopListPage.vue'], resolve)
            },
        },

        {
            path: '/Shop-List/:shop_zone',
            component: function (resolve) {
                require(['@/components/ShopList/ShopListPage.vue'], resolve)
            },
        },


        {
            path: '/Home',
            component: function (resolve) {
                require(['@/components/HomePage/HomePage.vue'], resolve)
            },
        },


        // {
        //     path: '/product-detail/:id_product',
        //     component: function (resolve) {
        //         require([
        //             '@/components/ProductInfo/ProductInfoPage.vue',
        //         ], resolve)
        //     },
        // },


    ],
})
export default router
