
//import { v4 as uuidv4 } from 'uuid'
const root = '/api'


export default {


    async getBackgroundImage() {
        const result = await fetch(`${root}/background-images`);
        return await result.json();
    },
    async loadAllProductList() {
        const result = await fetch(`${root}/allproducts/products`);
        return await result.json();
    },
    async getProductByCategories() {
        const result = await fetch(`${root}/productByCategory/products`);
        return await result.json();
    },
    async downloadrandomProductList() {
        const result = await fetch(`${root}/random_product/products`);
        return await result.json();
    },
    async loadProductsListFiltredBySearchedName(searchedWord) {
        const result = await fetch(
            `${root}/search-products-list-by-name/products/` + searchedWord
        );
        return await result.json();
    },
    async loadProductsFiltredByprincipalCategories(searchedPrincipalCategory) {
        const result = await fetch(
            `${root}/products/product-filter-principal-category/` + searchedPrincipalCategory
        );
        return await result.json();
    },
    async searchProductByName(searchedNameParams) {
        const result = await fetch(
            `${root}/search_product_by_name/products/` + searchedNameParams
        );
        return await result.json();
    },


    async loadProductsFiltredBySubCategories(searchedSubCategory) {
        const result = await fetch(
            `${root}/products/product-filter-sub-category/` + searchedSubCategory
        );
        return await result.json();
    },


    async loadProductDetails(searchedProduct) {
        const result = await fetch(
            `${root}/products/get-product-details/` + searchedProduct
        );
        return await result.json();
    },


    async loadShopMiniDetails(shop_id) {

        const result = await fetch(
            `${root}/shops/get-shop-mini-details/` + shop_id
        );
        return await result.json();


    },
    //---------------------categories ------------/ 
    async loadProductSubCategories(searchedPrincipalCategory) {
        const result = await fetch(
            `${root}/categories/all-sub-categories/` + searchedPrincipalCategory
        );
        return await result.json();
    },
    async loadProductprincipalCategories() {
        const result = await fetch(`${root}/all-principal-categories/categories`);
        return await result.json();
    },
    async downloadListOfprincipalCategories() {
        const result = await fetch(`${root}/all-principal-categories-details/categories`);
        return await result.json();
    },
    async downloadListOfSubCategories(searchedCategory) {
        const result = await fetch(`${root}/all-sub-categories-details/categories/` + searchedCategory);
        return await result.json();
    },
    async loadshopprincipalCategories() {
        const result = await fetch(`${root}/all-shop-principal-categories/categories`);
        return await result.json();
    },
    //---------------------shops ------------/ 
    async loadAllShopList() {
        const result = await fetch(`${root}/all-shops/shops`);
        return await result.json();
    },
    async loadShopsFiltredByprincipalCategories(searchedPrincipalCategory) {
        const result = await fetch(
            `${root}/shops/shops-filter-category/` + searchedPrincipalCategory
        );
        return await result.json();
    },
    async loadShopDetails(searchedShop) {
        const result = await fetch(
            `${root}/shops/get-shop-details/` + searchedShop
        );
        return await result.json();
    },
    async downloadrandomShopList() {
        const result = await fetch(`${root}/random_shops/shops`);
        return await result.json();
    },
    async loadThisZoneShopList(shop_zone) {
        const result = await fetch(
            `${root}/search_shops_by_zone/shops/` + shop_zone
        );
        return await result.json();
    },

}
