import Vue from 'vue'
import App from './App.vue'
import ImageUploader from 'vue-image-upload-resize'
import router from './routes'
import VueCarousel from 'vue-carousel'
import VueSingleSelect from 'vue-single-select'
import DynamicSelect from 'vue-dynamic-select'

import vuetify from '@/plugins/vuetify' // path to vuetify export


Vue.use(Vuetify)
Vue.use(vuetify)

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'



const opts = {}

export default new Vuetify(opts)
















Vue.use(DynamicSelect)

Vue.component('vue-single-select', VueSingleSelect)

Vue.use(VueCarousel)

Vue.use(ImageUploader)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  vuetify: new Vuetify(),

  icons: {
    iconfont: 'mdi', // default - only for display purposes
  },


  render: h => h(App),
  router,
}).$mount('#app')