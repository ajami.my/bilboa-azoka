import { shallowMount } from "@vue/test-utils";
import ShopDetailsPage from "@/components/ShopDetails/ShopDetailsPage.vue";
import ShowShopDetails from "@/components/ShopDetails/ShowShopDetails.vue";
import API from "@/API.js";

const finishAsyncTasks = () => new Promise(setImmediate);

test("loading data when mounted  ", async () => {
    API.loadShopDetails = jest.fn();

    const mockProduct = [
        {
            shop: {
                shop_id: "1",
                shop_adress: "numero 2",
                shop_name: "kala Moon",
                shop_zone: "Deustu"
            },
        }
    ];


    API.loadShopDetails.mockResolvedValue(mockProduct);

    const wrapper = shallowMount(ShopDetailsPage, {
        mocks: {
            $route: {
                params: { searchedShop: "1" }
            }
        },
        data() {
            return {
                searchedShop: this.$route.params.searchedShop,
                shopDetails: []
            };
        }
    });

    await finishAsyncTasks();

    expect(API.loadShopDetails).toHaveBeenCalled();

    expect(wrapper.vm.shopDetails).toEqual(mockProduct);
});
