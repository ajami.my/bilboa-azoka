import { shallowMount, createLocalVue } from "@vue/test-utils";
import ShowShopDetails from "@/components/ShopDetails/ShowShopDetails.vue";

test("Comprobamos que información de App imprimimos en la pantalla", () => {

    const wrapper = shallowMount(ShowShopDetails, {
        propsData: {
            shopDetails: {
                shop_id: "1",
                shop_adress: "numero 2",
                shop_street: "calle 3",
                shop_category: "ropa",
                shop_name: "kala Moon",
                shop_zone: "Deustu",
                shop_tel: "00000000",
            },
        }
    });
    expect(wrapper.text()).toContain("Direccion :numero 2");
    expect(wrapper.text()).toContain("Zona :Deustu");
    expect(wrapper.text()).toContain("Categoria :ropa");
    expect(wrapper.text()).toContain("kala Moon");
    expect(wrapper.text()).toContain("Calle :calle 3");
    expect(wrapper.text()).toContain("Telefono :00000000");
});

