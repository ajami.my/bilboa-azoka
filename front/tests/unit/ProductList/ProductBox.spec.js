import { shallowMount } from "@vue/test-utils";
import ProductBox from "@/components/ProductList/ProductBox.vue";

test("pinta en la pantalla", () => {
    const wrapper = shallowMount(ProductBox, {
        propsData: {
            productItem: {
                product_id: "1",
                product_name: "camisa",
                product_img: "image",
                product_price: "1",
            },
        }
    });

    expect(wrapper.text()).toContain("camisa");
    expect(wrapper.text()).toContain("1 Euro");
});


test(" that it send the event go-to-this-product-details with his argumenet searchedProduct ", async () => {

    const wrapper = shallowMount(ProductBox, {
        propsData: {
            productItem: {
                product_id: "1",
                product_name: "camisa",
                product_img: "image",
                product_price: "1",
            },

        },
        data() {
            return {

            };
        }
    });

    expect(wrapper.emitted().click).toBe(undefined);

    const seeMoreDetailsOnClick = wrapper.find("v-card");
    seeMoreDetailsOnClick.trigger("click");
    await wrapper.vm.$nextTick();

    expect(wrapper.emitted()["go-to-this-product-details"].length).toBe(1);
    expect(wrapper.emittedByOrder()).toEqual([
        {
            name: "go-to-this-product-details",
            args: ["1"]
        }
    ]);
});
