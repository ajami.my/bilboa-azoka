import { shallowMount } from "@vue/test-utils";
import ProductListPage from "@/components/ProductList/ProductListPage.vue";
import ProductBox from "@/components/ProductList/ProductBox.vue";
import API from "@/API.js";

const finishAsyncTasks = () => new Promise(setImmediate);

test("la function loadProductprincipalCategories for products ", async () => {
    API.loadProductprincipalCategories = jest.fn();
    API.ExecutFuncionalidadSelectPrincipalCategory = jest.fn();
    API.loadAllProductList = jest.fn();
    API.searchProductByName = jest.fn();
    API.loadProductsFiltredBySubCategories = jest.fn();
    const mockResultCategories = [
        {
            principal_category: "Todas"
        },
        {
            principal_category: "casa"
        },
        {
            principal_category: "ropa"
        },

    ];
    API.loadProductprincipalCategories.mockReturnValue(mockResultCategories);

    const wrapper = shallowMount(ProductListPage, {
        mocks: {
            $route: {
                params: [
                    { searchedSubCategory: undefined },
                    { searchedProductName: undefined },
                    { filtredPrincipalCategory: undefined }
                ]
            }
        },
        data() {
            return {
                productprincipalCategories: []
            };
        }
    });

    await finishAsyncTasks();

    expect(API.loadProductprincipalCategories).toHaveBeenCalled();

    expect(wrapper.vm.productprincipalCategories).toEqual(mockResultCategories);
});


test("condition en the mounted params.filtredPrincipalCategory !== undefined  and filtredPrincipalCategory: 'Todas' ", async () => {
    API.ExecutFuncionalidadSelectPrincipalCategory = jest.fn();
    API.loadAllProductList = jest.fn();
    API.searchProductByName = jest.fn();
    API.loadProductsFiltredBySubCategories = jest.fn();
    API.loadProductSubCategories = jest.fn();

    const mockResultProducts = [
        {
            product_category: "ropa",
            product_description: "description 1",
            product_id: "1",
            product_name: "camisa",
            product_img: "image",
            product_price: "1",
            product_sub_category: "mujer",
            stat: "disponible"

        },
        {
            product_category: "food",
            product_description: "description 2",
            product_id: "2",
            product_name: "platano",
            product_img: "image",
            product_price: "2",
            product_sub_category: "fruta",
            stat: "disponible"
        },
        {
            product_category: "casa",
            product_description: "description 3",
            product_id: "3",
            product_name: "cucharita",
            product_img: "image",
            product_price: "3",
            product_sub_category: "cocina",
            stat: "disponible"
        },

    ];
    const mockResultCategories = [
        {
            principal_category: "Todas"
        },
        {
            principal_category: "casa"
        },
        {
            principal_category: "ropa"
        },
    ];
    const mockResultSubCategory = [
        {
            principal_category: "Todas"
        },
        {
            principal_category: "casa"
        },
        {
            principal_category: "ropa"
        },

    ];
    API.loadAllProductList.mockReturnValue(mockResultProducts);
    API.loadProductSubCategories.mockReturnValue(mockResultSubCategory);
    API.loadProductprincipalCategories.mockReturnValue(mockResultCategories);
    const wrapper = shallowMount(ProductListPage, {
        mocks: {
            $route: {
                params: {
                    searchedSubCategory: undefined,
                    searchedProductName: undefined,
                    filtredPrincipalCategory: 'Todas'
                }
            }
        },
        data() {
            return {
                productList: null,
                searchedPrincipalCategory: "principal-categorias",
            };
        }
    });


    await finishAsyncTasks();

    expect(API.searchProductByName).toHaveBeenCalledTimes(0);
    expect(API.loadProductsFiltredBySubCategories).toHaveBeenCalledTimes(0);

    expect(API.loadAllProductList).toHaveBeenCalled();


    expect(wrapper.vm.productList).toEqual(mockResultProducts);


    const productBox = wrapper.findAll(ProductBox).wrappers;

    expect(productBox.length).toEqual(3);
});


test("condition en the mounted params.filtredPrincipalCategory !== undefined  and filtredPrincipalCategory is not  'Todas' ", async () => {
    API.ExecutFuncionalidadSelectPrincipalCategory = jest.fn();
    API.loadAllProductList = jest.fn();
    API.searchProductByName = jest.fn();
    API.searchProductByName = jest.fn();
    API.loadProductsFiltredByprincipalCategories = jest.fn();
    API.loadProductSubCategories = jest.fn();
    API.loadProductprincipalCategories = jest.fn();
    API.loadProductsFiltredBySubCategories = jest.fn();

    const mockResultProducts = [
        {
            product_category: "ropa",
            product_description: "description 1",
            product_id: "1",
            product_name: "camisa",
            product_img: "image",
            product_price: "1",
            product_sub_category: "mujer",
            stat: "disponible"

        },
    ];
    const mockResultCategory = [
        {
            principal_category: "Todas"
        },
        {
            principal_category: "casa"
        },
        {
            principal_category: "ropa"
        },
    ];
    const mockResultSubCategory = [
        {
            principal_category: "mujer"
        },
        {
            principal_category: "hombre"
        },
        {
            principal_category: "niño"
        },
    ];

    API.loadProductsFiltredByprincipalCategories.mockReturnValue(mockResultProducts);
    API.loadProductSubCategories.mockReturnValue(mockResultSubCategory);
    API.loadProductprincipalCategories.mockReturnValue(mockResultCategory);


    const wrapper = shallowMount(ProductListPage, {
        mocks: {
            $route: {
                params: {

                    filtredPrincipalCategory: 'ropa'
                }
            }
        },
        data() {
            return {
                productList: [],
                searchedPrincipalCategory: "principal-categorias",
                shopCategories: [],
                productprincipalCategories: [],
                productSubCategories: [],
                searchedSubCategory: "sub-category",
                searchedProductsName: "null",
                principalCategoriesItems: [],
                subCategoriesItems: [],
                items: [],
                showMessage: false,
                showTheGallery: true,
                showCategory: false
            };
        }
    });
    await finishAsyncTasks();
    expect(API.searchProductByName).toHaveBeenCalledTimes(0);
    expect(API.loadAllProductList).toHaveBeenCalledTimes(0);
    expect(API.loadProductsFiltredByprincipalCategories).toHaveBeenCalledWith('ropa');
    expect(wrapper.vm.productList).toEqual(mockResultProducts);
    const productBox = wrapper.findAll(ProductBox).wrappers;
    expect(productBox.length).toEqual(1);
});



test("condition en the mounted searchedProductName!== undefined   ", async () => {
    API.ExecutFuncionalidadSelectPrincipalCategory = jest.fn();
    API.loadAllProductList = jest.fn();
    API.searchProductByName = jest.fn();
    API.loadProductsFiltredByprincipalCategories = jest.fn();
    API.loadProductSubCategories = jest.fn();
    API.loadProductprincipalCategories = jest.fn();
    API.loadProductsFiltredBySubCategories = jest.fn();

    const mockResultProducts = [
        {
            product_category: "ropa",
            product_description: "description 1",
            product_id: "1",
            product_name: "camisa",
            product_img: "image",
            product_price: "1",
            product_sub_category: "mujer",
            stat: "disponible"

        },
    ];
    const mockResultCategory = [
        {
            principal_category: "Todas"
        },
        {
            principal_category: "casa"
        },
        {
            principal_category: "ropa"
        },
    ];
    const mockResultSubCategory = [
        {
            principal_category: "mujer"
        },
        {
            principal_category: "hombre"
        },
        {
            principal_category: "niño"
        },
    ];

    API.searchProductByName.mockReturnValue(mockResultProducts);
    API.loadProductSubCategories.mockReturnValue(mockResultSubCategory);
    API.loadProductprincipalCategories.mockReturnValue(mockResultCategory);


    const wrapper = shallowMount(ProductListPage, {
        mocks: {
            $route: {
                params: {

                    searchedProductName: 'camisa'
                }
            }
        },
        data() {
            return {
                productList: [],
                searchedPrincipalCategory: "principal-categorias",
                shopCategories: [],
                productprincipalCategories: [],
                productSubCategories: [],
                searchedSubCategory: "sub-category",
                searchedProductsName: "null",
                principalCategoriesItems: [],
                subCategoriesItems: [],
                items: [],
                showMessage: false,
                showTheGallery: true,
                showCategory: false
            };
        }
    });
    await finishAsyncTasks();
    expect(API.loadProductsFiltredByprincipalCategories).toHaveBeenCalledTimes(0);
    expect(API.loadAllProductList).toHaveBeenCalledTimes(0);

    expect(wrapper.vm.searchedNameParams).toBe('camisa');
    expect(API.searchProductByName).toHaveBeenCalledWith('camisa');
    expect(wrapper.vm.productList).toEqual(mockResultProducts);
    const productBox = wrapper.findAll(ProductBox).wrappers;
    expect(productBox.length).toEqual(1);
});
