import { shallowMount } from "@vue/test-utils";
import ShopBox from "@/components/ShopList/ShopBox.vue";

test("pinta en la pantalla", () => {
    const wrapper = shallowMount(ShopBox, {
        propsData: {
            shop: {
                shop_id: "1",
                shop_adress: "numero 2",
                shop_name: "kala Moon",
                shop_zone: "Deustu"
            },
        }
    });

    expect(wrapper.text()).toContain("kala Moon");
    expect(wrapper.text()).toContain("Deustu");
});

test(" that it send the event go-to-this-shop-details with his argumenet shop_id ", async () => {

    const wrapper = shallowMount(ShopBox, {
        propsData: {
            shop: {
                shop_id: "1",
                shop_adress: "numero 2",
                shop_name: "kala Moon",
                shop_zone: "Deustu",
                shop_img: "image"
            },
        },
        data() {
            return {
                searchedShopId: null
            };
        }
    });

    expect(wrapper.emitted().click).toBe(undefined);

    const seeMoreDetailsOnClick = wrapper.find(".shop-card");
    seeMoreDetailsOnClick.trigger("click");
    await wrapper.vm.$nextTick();

    expect(wrapper.emitted()["go-to-this-shop-details"].length).toBe(1);
    expect(wrapper.emittedByOrder()).toEqual([
        {
            name: "go-to-this-shop-details",
            args: ["1"]
        }
    ]);
});
