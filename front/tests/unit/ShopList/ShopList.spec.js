import { shallowMount } from "@vue/test-utils";
import ShopListPage from "@/components/ShopList/ShopListPage.vue";
import ShopBox from "@/components/ShopList/ShopBox.vue";
import API from "@/API.js";

const finishAsyncTasks = () => new Promise(setImmediate);

test("la function loadshopprincipalCategories for shops  ", async () => {
    API.loadshopprincipalCategories = jest.fn();
    API.loadAllShopList = jest.fn();
    API.loadThisZoneShopList = jest.fn();
    const mockResultUsers = [
        {
            principal_category: "Todas"
        },
        {
            principal_category: "casa"
        },
        {
            principal_category: "ropa"
        },

    ];
    API.loadshopprincipalCategories.mockReturnValue(mockResultUsers);

    const wrapper = shallowMount(ShopListPage, {
        mocks: {
            $route: {
                params: { shop_zone: 'undefined' }
            }
        },
        data() {
            return {
                shopCategories: []
            };
        }
    });

    await finishAsyncTasks();

    expect(API.loadshopprincipalCategories).toHaveBeenCalled();

    expect(wrapper.vm.shopCategories).toEqual(mockResultUsers);
});


test("it doesnt use loadThisZoneShopList when params is undefined", async () => {

    API.loadAllShopList = jest.fn();
    API.loadThisZoneShopList = jest.fn();
    const mockResultUsers = [
        {
            shop_adress: "numero 2",
            shop_id: "1",
            shop_tel: "0000000",
            shop_zone: "Deustu"
        },
        {
            shop_adress: "numero 3",
            shop_id: "2",
            shop_tel: "0005555",
            shop_zone: "Deustu"
        },
        {
            shop_adress: "numero 5",
            shop_id: "3",
            shop_tel: "008888",
            shop_zone: "Abando"
        },

    ];
    API.loadAllShopList.mockReturnValue(mockResultUsers);

    const wrapper = shallowMount(ShopListPage, {
        mocks: {
            $route: {
                params: { shop_zone: undefined }
            }
        },
        data() {
            return {
                shopList: []
            };
        }
    });


    await finishAsyncTasks();

    expect(API.loadThisZoneShopList).toHaveBeenCalledTimes(0);
    expect(API.loadAllShopList).toHaveBeenCalled();


    expect(wrapper.vm.shopList).toEqual(mockResultUsers);


    const shopBox = wrapper.findAll(ShopBox).wrappers;

    expect(shopBox.length).toEqual(3);
});