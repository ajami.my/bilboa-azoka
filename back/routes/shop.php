<?php

Route::get('/shops/get-shop-mini-details/{shop_id}', function ($shop_id) {

    $results = DB::select('select shop_id, shop_name, shop_img from shops where shop_id=:shop_id ', [
        'shop_id' => $shop_id,
    ]);
    return response()->json($results[0], 200);
});

Route::get('/all-shops/shops', function (Request $request) {
    $results = DB::select('select shop_id, shop_name,shop_adress,shop_zone, shop_street, shop_tel, shop_img, shop_category from shops ');
    return response()->json($results, 200);
});

Route::get('/shops/shops-filter-category/{shop_category}', function ($shop_category) {

    $results = DB::select('select shop_id, shop_name,shop_adress,shop_zone, shop_street, shop_tel, shop_img from shops where shop_category=:shop_category ',
        [
            'shop_category' => $shop_category,
        ]);
    return response()->json($results, 200);
});

Route::get('/shops/get-shop-details/{shop_id}', function ($shop_id) {

    $results = DB::select('select * from shops where shop_id=:shop_id ', [
        'shop_id' => $shop_id,
    ]);

    DB::update("update shops set  views=views+1 where shop_id=:shop_id",
        [
            'shop_id' => $shop_id,
        ]);
    return response()->json($results[0], 200);
});

Route::get('/random_shops/shops', function (Request $request) {

    $results = DB::select('SELECT * FROM shops ORDER BY RANDOM() ');
    return response()->json($results, 200);
});

Route::get('/search_shops_by_zone/shops/{shop_zone}', function ($shop_zone) {

    $results = DB::select('select * from shops where shop_zone=:shop_zone ', [
        'shop_zone' => $shop_zone,
    ]);

    DB::update("update zones set  zone_views=zone_views+1 where shop_zone=:shop_zone",
        [
            'shop_zone' => $shop_zone,
        ]);
    return response()->json($results, 200);
});
