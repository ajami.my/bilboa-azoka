<?php

Route::get('/allproducts/products', function (Request $request) {
    $results = DB::select('select product_id, product_name,product_category,product_sub_category, product_description, product_price, product_img, stat  from products ');
    return response()->json($results, 200);
});

Route::get('/search-products-list-by-name/products/{txt}', function ($txt) {

    $results = DB::select('select product_id as id, product_name as name from products WHERE stat="disponible" and product_name LIKE "%' . $txt . '%" ');
    return response()->json($results, 200);
});

Route::get('/random_product/products', function (Request $request) {

    $results = DB::select('SELECT * FROM products ORDER BY RANDOM() ');
    return response()->json($results, 200);
});

Route::get('/search_product_by_name/products/{text}', function ($text) {

    $results = DB::select("select product_id, product_name, product_price, product_img from products where product_name LIKE '$text%' "
   

);
    return response()->json($results, 200);
});

Route::get('/products/product-filter-principal-category/{product_category}', function ($product_category) {

    $results = DB::select("select * from products where product_category=:product_category and stat='disponible'",
        [
            'product_category' => $product_category,
        ]);
    return response()->json($results, 200);
});

Route::get('/products/product-filter-sub-category/{product_category}', function ($product_category) {

    $results = DB::select('select * from products where product_sub_category=:product_category and stat="disponible"',
        [
            'product_category' => $product_category,
        ]);
    return response()->json($results, 200);
});

Route::get('/products/get-product-details/{product_id}', function ($product_id) {

    if (productNoExists($product_id)) {
        abort(404);
    }
    $results = DB::select('select * from products where product_id=:product_id ', [
        'product_id' => $product_id,
    ]);

    DB::update("update products set  views=views+1 where product_id=:product_id",
        [
            'product_id' => $product_id,
        ]);

    return response()->json($results[0], 200);
});

Route::get('/my_products/{shop_id}', function ($shop_id) {

    $results = DB::select('select product_id, product_name, product_category, product_description, product_price, product_img  from products where shop_id=:shop_id ',
        [
            'shop_id' => $shop_id,
        ]);
    return response()->json($results, 200);
});

Route::post('/products', function () {

    $data = request()->all();
    DB::insert(
        "
        INSERT into products (product_id, product_name, product_category, product_description, product_price, product_img, shop_id)
        VALUES (:product_id, :product_name, :product_category, :product_description, :product_price, :product_img, :shop_id)
    ",
        $data
    );

    $results = DB::select('select * from products where product_id = :product_id', [
        'product_id' => $data['product_id'],
    ]);
    return response()->json($results[0], 200);
});
Route::PUT('/products/{product_id}', function (Request $request, $product_id) {
    if (productNoExists($idproduct)) {
        abort(404);
    }

    DB::update("update products set  product_name=?, product_category=?,  product_description=?,  product_price=? ,  product_img=?,  where product_id=?",
        [$request->product_name,
            $request->product_category,
            $request->product_description,
            $request->product_price,
            $request->product_img,

            $request->product_id]);
    $results = DB::select('select * from products where product_id = :product_id', ['product_id' => $product_id]);
    return response()->json($results[0], 200);
});

Route::get('/all-principal-categories/categories', function (Request $request) {
    $categories = DB::select('select DISTINCT principal_category from productCategories ');

    foreach ($categories as $category) {

        $productsByCategory = DB::select('select * from products where product_category=:product_category and stat="disponible"',
            [
                'product_category' => $category,
            ]);
    }

    return response()->json($productsByCategory, 200);
});

Route::get('/productByCategory/products', function (Request $request) {
    $categoriesList = DB::select('select DISTINCT principal_category from productCategories ');

    $productsByCategory = [];
    foreach ($categoriesList as $category) {

        $principal_category = $category->principal_category;

        $results = DB::select('select product_name, product_img, product_category from products where product_category=:principal_category and stat="disponible" ORDER BY RANDOM() desc LIMIT 0,4',
            [
                'principal_category' => $principal_category,
            ]);

        $resultssize = sizeof($results);
        if ($resultssize > 0) {

            $productsByCategory[$principal_category] = $results;
        }
    }

    return response()->json($productsByCategory, 200);

});

if (!function_exists('productNoExists')) {
    function productNoExists($product_id)
    {
        $results = DB::select('select * from products where product_id=:product_id', [
            'product_id' => $product_id,
        ]);
        return count($results) == 0;
    }
}
