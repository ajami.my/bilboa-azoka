<?php

Route::get('/all-principal-categories/categories', function (Request $request) {
    $results = DB::select('select DISTINCT principal_category from productCategories ');
    return response()->json($results, 200);
});

Route::get('/categories/all-sub-categories/{product_category}', function ($product_category) {

    $results = DB::select('select DISTINCT sub_category  from productCategories where principal_category=:product_category and sub_category!="null"',
        [
            'product_category' => $product_category,
        ]);
    return response()->json($results, 200);
});

Route::get('/all-principal-categories-details/categories', function (Request $request) {
    $results = DB::select('select principal_category, principal_icon from productCategories where sub_category="null" ');
    return response()->json($results, 200);
});

Route::get('/all-sub-categories-details/categories/{principal_category}', function ($principal_category) {
    $results = DB::select('select sub_category, sub_icon from productCategories where principal_category=:principal_category and sub_category!="null"',
        [
            'principal_category' => $principal_category,
        ]);

    return response()->json($results, 200);
});
